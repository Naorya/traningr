#-------------------------------------Ex-8 tree model------------------------------
install.packages("ISLR")
library(ISLR)
library(ggplot2)
library(dplyr)
install.packages('rpart.plot')
#-------------------------------------------------------------------------------------
df <- College
str(df)
#Plot Room.Board against Grad.Rate while color is Private what is the conclusion? 
#Room and board costs vs Graduation rate
pl <- ggplot(df, aes(Room.Board,Grad.Rate)) + geom_point(aes(color = Private), size = 4, alpha = 0.5)#אוניברסיטה פרטית או ציבורית

#Plot a histogram of Grad.Rate and show how Private is distributed in each bar what is the conclusion? 
pl1 <- ggplot(df, aes(x = Grad.Rate))+geom_histogram(aes(fill = Private), color = 'black', bins =50)

#Plot a histogram of F.Undergrad and show how Private is distributed in each bar what is the conclusion?  
pl2 <- ggplot(df, aes(x = Grad.Rate))+ geom_histogram(aes(fill = Private), color = 'black', bins =50)

#Fix the problem of schools with Grad.Rate above 100%

df['Cazenovia College', 'Grad.Rate'] <- 100#יש לו 118 ולגן נזריק לו 10
filter <- df[,"Grad.Rate"] >= 100#מה שמעל 100 או שווה לו יקבל טרו 
df[filter,]

#Divide the data into train set and test set 
library(caTools)
sample <- sample.split(df, SplitRatio = 0.7)
df.train <- subset(df, sample ==T)
df.test <- subset(df, sample ==F)
dim(df)
dim(df.train)
dim(df.test)
#Derive the tree model
library(rpart)
tree <- rpart(Private ~ .,method = 'class' ,data = df.train)

library(rpart.plot)

#plot the tree model
prp(tree)
tree.preds <- predict(tree,df.test)#יש הרבה רשומות ולכן נשתמש בטסט
head(tree.preds)
tree.preds <- as.data.frame(tree.preds)#נהפוך את זה לדאגא פריים

#preapre to confusion matrix, Recall and Precision
joiner <- function(x){
  if(x > 0.5){
    return('PYes')
  }else{
    return('PNo')
  }
} 
tree.preds$private <- sapply(tree.preds$Yes, joiner)#תרוץ על עמודת ה-יס בטבלה ותפעיל את הפונקציה  גוניר
head(tree.preds)#check if the coulmn added

#חישוב ה- confusion matrix 
confusion <- table(tree.preds$private, df.test$Private)
class(confusion)
head(df.test$Private)
#Recall and Precision
TN <- confusion[1,1]
FN <- confusion[1,2]
FP <- confusion[2,1]
TP <- confusion[2,2]

recall <- TP/(TP+FN)#הצלחנו לחזות 92% נכון שיעזבו
precision <- TP/(TP+FP)#מתוך כל אלה שהמודל סימן שיעזבו 96% עוזבים 








